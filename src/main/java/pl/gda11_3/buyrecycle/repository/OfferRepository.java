package pl.gda11_3.buyrecycle.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import pl.gda11_3.buyrecycle.model.Offer;

import java.util.List;
import java.util.Optional;

@Repository
public interface OfferRepository extends JpaRepository<Offer, Long> {

//    @Query("SELECT * FROM Offer o JOIN Material m on o.material=m.id where m.name=materialName")
//    Optional<Offer> findByname(@Param("materialName")String materialName);

    List<Offer> findAllByOfferNameContaining(String pSearch);
}
